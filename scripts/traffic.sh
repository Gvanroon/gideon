#!/bin/bash


#Import parameters
SCRIPT_PATH=$(readlink -f "$0")
SCRIPT_DIR=$(dirname "$SCRIPT_PATH")
. $SCRIPT_DIR"/parameters"

(crontab -l >/dev/null; echo "$CRON_CONFIG $SCRIPT_DIR/poll_traffic.sh") | crontab -


export ARRAY_TIME=()
export ARRAY_JAM=()

columns=$(($(wc -l data | cut -d ' ' -f1) - 1))
rows=$(((($RANGE_MAX - $RANGE_MIN) / $STEP) - 1))

tail -n $(($(tput cols)-12)) data > dat
while read time jam
do
	ARRAY_TIME+=("$time")
	ARRAY_JAM+=("$jam")
done < dat

rm dat


OLD_IFS=$IFS
IFS=$'\n'


### Rendering image ###

image=()

previous=$(((${ARRAY_JAM[0]}/$STEP)*$STEP))
current=0
null=0
col=0
time_previous=0
line_last="  "
line_second_last="+-"
line_draw_lock=0

## determine graph element ##

for jam in ${ARRAY_JAM[@]}
do
	current=$jam
	difference=$(expr $current - $previous)

	if [[ $difference -lt 0 ]]; then
		difference_abs=$(expr 0 - $difference)
	else
		difference_abs=$difference
	fi
	
	if [[ $difference_abs -gt $(expr $STEP / 2) ]]; then
		if [[ $difference -lt 0 ]]; then
			char="\\"
			previous=$(($previous-$STEP))
			offset_fix=$(($previous+$STEP))
		fi
	fi
	if [[ $difference_abs -gt $(($STEP / 2)) ]]; then
		if [[ $difference -gt 0 ]]; then
			char="/"
			previous=$(($previous+$STEP))
			offset_fix=$previous
		fi	
	fi
	if [[ $difference_abs -le $(expr $STEP / 2) ]]; then
		char="_"
		offset_fix=$(($previous+$STEP))
	fi
	
	#previous=$current

	time_current=$( echo "${ARRAY_TIME[$col]}" | cut -d ':' -f1)
	
	if [[ line_draw_lock -le 0 ]]; then
		if [[ ${time_current#0} -ne ${time_previous#0} ]]; then
			line_draw_lock=3
			line_last="$line_last${time_current}h "
			line_second_last="$line_second_last+"
		else	
			line_last="${line_last} "
			line_second_last="$line_second_last-"
		fi
	else
		line_draw_lock=$(($line_draw_lock-1))
		line_second_last="$line_second_last-"
	fi
	
	time_previous=$time_current


## Fill characters in image ##

	for row in $(seq 0 $rows);
	do 	
		index=$((($row * ($columns + 1)) + $col))

		if [[ $(($offset_fix - ($RANGE_MAX - ($row * $STEP)))) -ge 0 ]] && [[ $(($offset_fix - ($RANGE_MAX - ($row * $STEP)))) -lt $STEP ]]; then
			image[$index]=$char
			last_row=$row	
		else
			image[$index]=" "				
		fi	
	done	
	col=$(($col + 1))
done

### Plotting image ###

col=0

clear
printf "Realtime traffic:\n" | tee data.py

for row in $(seq 0 $rows);
do 	
	line="| "
	index_row=$(($row * ($columns + 1)))
	
	for col in $(seq 0 $columns);
	do 	
		index=$(($index_row + $col))
		line="$line${image[$index]}"
	done

	if [[ $row -eq $last_row ]]; then
		printf "$line  < $current km\n" | tee -a data.py
	else
		printf "$line\n" | tee -a data.py
	fi

done



### Displaying stuff ###

printf "$line_second_last\n" | tee -a data.py
printf "$line_last\n" | tee -a data.py
