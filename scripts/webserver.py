#!/usr/bin/env python3

from http.server import HTTPServer, BaseHTTPRequestHandler
from parameters import *


class Handler(BaseHTTPRequestHandler):
    def do_GET(self):
        print("### responding to GET")
        
        self.send_response(200)
        self.send_header('Content-Type','text/plain')
        self.end_headers()
 
        dataFile = open("data.py","r")
        content = dataFile.readlines()
        dataFile.close()
        
        for line in content:
            self.wfile.write(line.encode('utf-8'))

if __name__ == '__main__':
    try:
        print("### starting server...")
        server = HTTPServer((HOST, PORT), Handler)
        server.serve_forever()

    except KeyboardInterrupt:
        print("### stopping server...")
        server.socket.close()
