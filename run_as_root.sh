#!/bin/sh

USERNAME="debian"

SCRIPT_PATH=$(readlink -f "$")
SCRIPT_DIR=$(dirname "SCRIPT_PATH")

GIT_REPO="https://www.gitlab.com/gvanroon/gideon"
GIT_DIR="/install_script/"
GIT_SCRIPT_DIR=$GIT_DIR"gideon/"

apt update && apt install -y \
	sudo \
	vim \
	git \
	python3-pip

dpkg --configure -a
apt install -f -y

echo "#####   ADD USER TO SUDOERS   #####"

/usr/sbin/usermod -aG sudo $USERNAME
adduser $USERNAME sudo

mkdir $GIT_DIR && cd $GIT_DIR
git clone $GIT_REPO

echo "#####   RUNNING SCRIPTS   #####"

$GIT_SCRIPT_DIR"./gideon_settings.sh root"
$GIT_SCRIPT_DIR"./docker_install.sh"
$GIT_SCRIPT_DIR"./guestAdditions_install.sh"

echo "#####   CLEANING   #####"

rm -rf $GIT_DIR

echo "#####   SWITCHING USER #####"

exec su -l $USERNAME

$GIT_SCRIPT_DIR"./gideon_settings.sh root user"
