#!/bin/sh

echo "#####   Check if running in VM   #####"

VBOX_VERSION="5.2.22"

ISO_URL="http://download.virtualbox.org/virtualbox"

DEB_PATH="/VBox_guest_additions_package/"

apt update && apt upgrade -y

apt install -y lshw

if lshw -class system | grep "VirtualBox" ; then

    echo "  ###   Running in VirtualBox   ###"

	apt install -y build-essential module-assistant curl
	
	mkdir $DEB_PATH

	DISTRO_ID=$(lsb_release -i | cut -d : -f2 | sed -e 's/^\s*//' -e '/^$/d')
	CODENAME=$(lsb_release -c | cut -d : -f2 | sed -e 's/^\s*//' -e '/^$/d')
	ARCH=$(lshw -class system | grep "width:" | sed -e 's/[^0-9]//g')

	VBOX_FILE=$(curl $ISO_URL"/"$VBOX_VERSION"/" | cat | grep $DISTRO_ID | grep -e "[A-Za-z]$ARCH" | grep $CODENAME | cut -d '"' -f2)
	
	echo "  ###   Downloading guest additions   ###"
	
	wget -O $DEB_PATH$VBOX_FILE $ISO_URL"/"$VBOX_VERSION"/"$VBOX_FILE
	dpkg --configure -a
	apt install -f -y

	m-a prepare

    echo "  ###   Installing guest additions   ###"

	apt install -y $DEB_PATH$VBOX_FILE

	rm -rf $DEB_PATH
fi


