# Gideon

GIDEON REPO

ADD fim alias

```shell
Moved to fim Repo

```



ADD TO BASHRC 
```shell
    PS1="\[\e]0;\u@\h: \w\a\]${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u\$(if [ \$? = 0 ]; then echo @; else echo \[\e[31m\]X; fi)\[\033[01;32m\]\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ "
    PROMPT_DIRTRIM=3
```

ADD vim plugins;
gruvbox, syntastic(for python; pylint)

ADD tmux

