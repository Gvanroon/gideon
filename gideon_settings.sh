#!/bin/sh

if [ "$1" = "root" ]
then

	git config --global user.email "gvanroon@nalys-group.com"
	git config --global user.name "Gideon van Roon"

	echo "#####   INSTALLING MORE STUFF   #####"

	apt update && apt install -y \
		htop \
		shellcheck \
		ripgrep \
		inotify-tools

	echo "#####   INSTALLING CTOP   #####"

	wget https://github.com/bcicen/ctop/releases/download/v0.7.1/ctop-0.7.1-linux-amd64 -O /usr/local/bin/ctop
	chmod +x /usr/local/bin/ctop

fi

if [ "$1" = "user" ]
then

	echo "#####   SETTING ALIASES   #####"

	echo "alias gitcred='git config credential.helper store'" >> ~/.bash_aliases
	echo "alias ll='ls -la'" >> ~/.bash_aliases
	echo "alias doccheck='docker run -i --rm hadolint/hadolint < Dockerfile'" >> ~/.bash_aliases

fi
