#!/bin/sh

echo "#####   INSTALLING DOCKER   #####"

echo "  ###   REMOVING OLD FILES   ###"
apt remove -y \
	docker \
	docker-engine \
	docker.io \
	containerd \
	runc 

dpkg --configure -a
apt install -f -y

echo "  ###   Installing dependencies   ###"

apt install -y \
	apt-transport-https \
	ca-certificates \
	curl \
	gnupg2
	
apt install -y software-properties-common

echo "   ###   Adding key   ###"

curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -

echo "   ###   Adding apt repo   ###"

add-apt-repository \
	"deb [arch=amd64] https://download.docker.com/linux/debian \
	$(lsb_release  -cs) \
	stable"

apt update && apt install -y docker-ce

groupadd docker
usermod -aG docker debian

curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
